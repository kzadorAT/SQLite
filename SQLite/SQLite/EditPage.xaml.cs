﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SQLite
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditPage : ContentPage
    {
        private Empleado empleado;

        public EditPage(Empleado empleado)
        {
            InitializeComponent();

            this.empleado = empleado;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    Padding = new Thickness(10, 20, 10, 10);
                    break;
                default:
                    Padding = new Thickness(10);
                    break;
            }

            nombresEntry.Text = empleado.Nombres;
            apellidosEntry.Text = empleado.Apellidos;
            salarioEntry.Text = empleado.Salario.ToString();
            fechaContratoDatePicker.Date = empleado.FechaContrato;
            activoSwitch.IsToggled = empleado.Activo;

            actualizarButton.Clicked += ActualizarButton_Clicked;
            borrarButton.Clicked += BorrarButton_Clicked;
        }

        private async void BorrarButton_Clicked(object sender, EventArgs e)
        {
            var rta = await DisplayAlert("Confirmación", "¿Desea borrar el empleado?", "Si", "No");
            if (!rta)
            {
                return;
            }

            using (var datos = new DataAccess())
            {
                datos.DeleteEmpleado(empleado);
            }

            await DisplayAlert("Confirmación", "Empleado borrado correctamente", "Aceptar");
            await Navigation.PopAsync();
        }

        private async void ActualizarButton_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nombresEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar nombres", "Aceptar");
                nombresEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(apellidosEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar apellidos", "Aceptar");
                apellidosEntry.Focus();
                return;
            }

            if (string.IsNullOrEmpty(salarioEntry.Text))
            {
                await DisplayAlert("Error", "Debe ingresar salario", "Aceptar");
                salarioEntry.Focus();
                return;
            }

            empleado.Nombres = nombresEntry.Text;
            empleado.Apellidos = apellidosEntry.Text;
            empleado.FechaContrato = fechaContratoDatePicker.Date;
            empleado.Salario = decimal.Parse(salarioEntry.Text);
            empleado.Activo = activoSwitch.IsToggled;

            using (var datos = new DataAccess())
            {
                datos.UpdateEmpleado(empleado);
            }

            await DisplayAlert("Confirmación", "Empleado actualizado correctamente", "Aceptar");
            await Navigation.PopAsync();
        }
    }
}