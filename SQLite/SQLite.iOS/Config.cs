﻿using SQLite.Net.Interop;
using System;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLite.iOS.Config))]

namespace SQLite.iOS
{
    public class Config : IConfig
    {
        private string direcorioDB;
        private ISQLitePlatform plataforma;

        public string DirectorioDB
        {
            get
            {
                if (string.IsNullOrEmpty(direcorioDB))
                {
                    var directorio = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    direcorioDB = System.IO.Path.Combine(directorio, "..", "Library");
                }

                return direcorioDB;
            }
        }

        public ISQLitePlatform Plataforma
        {
            get
            {
                if(plataforma == null)
                {
                    plataforma = new SQLite.Net.Platform.XamarinIOS.SQLitePlatformIOS();

                }

                return plataforma;
            }
        }
    }
}